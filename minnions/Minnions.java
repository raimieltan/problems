//solution
public class Minnions {
    public static int nbYear(int population, double percent, int berks, int target) {
        int years = 0;
        while (population < target) {
            population += population * percent / 100 + berks;
            years++;
        }
        return years;
    }
}

//template
public class Minnions {
    public static int nbYear(int population, double percent, int berks, int target) {
        //code here
    }
}

//test cases

import static org.junit.Assert.*;

import org.junit.Test;

public class MinnionsTest {

    Minnions minnions = new Minnions();

    @Test
    public void test___SE___case_1() {
 
        assertEquals(minnions.nbYear(1500, 5, 100, 5000),15);
    }

    @Test
    public void test___SE___case_2() {
 
        assertEquals(minnions.nbYear(1500000, 2.5, 10000, 2000000), 10);
    }

    @Test
    public void test___SE___case_3() {
        assertEquals(minnions.nbYear(1500000, 0.25, 1000, 2000000), 94);
    }

    @Test
    public void test___SE___case_4() {
        assertEquals(minnions.nbYear(1500000, 0.25, -1000, 2000000), 151);

    }

    @Test
    public void test___SE___case_5() {
        assertEquals(minnions.nbYear(1500000, 0.25, 1, 2000000), 116);
    }

    @Test
    public void test___SE___case_6() {
        assertEquals(minnions.nbYear(1500000, 0.0, 10000, 2000000), 50);
    }

}