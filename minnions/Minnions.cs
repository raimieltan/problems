//solution
using System;

class Minnions {
    
    public static int NbYear(int population, double percent, int berks, int taget) {
        int year;
        for (year = 0; population < target; year++)
          population += (int)(population*percent/100) + berks;
        return year;
    }
}

//template

class Minnions {
    
    public static int NbYear(int population, double percent, int berks, int taget) {
        //code here
    }
}

[Test]
    public static void test___SE___case_1() {

        Assert.AreEqual(Minnions.NbYear(1500, 5, 100, 5000), 15);
    }
[Test]
    public static void test___SE___case_2() {
        Assert.AreEqual(Minnions.NbYear(1500000, 2.5, 10000, 2000000), 10);
    }
[Test]
    public static void test___SE___case_3() {
        Assert.AreEqual(Minnions.NbYear(1500000, 0.25, 1000, 2000000), 94);
    }
[Test]
    public static void test___SE___case_4() {
        Assert.AreEqual(Minnions.NbYear(1500000, 0.25, -1000, 2000000), 151);
    }
[Test]
    public static void test___SE___case_5() {
        Assert.AreEqual(Minnions.NbYear(1500000, 0.25, 1, 2000000), 116);
    }
[Test]
    public static void test___SE___case_6() {
        Assert.AreEqual(Minnions.NbYear(1500000, 0.0, 10000, 2000000), 50);
    }
[Test]
    public static void test___SE___case_7() {
        Assert.AreEqual(Minnions.NbYear(1000, 2.0, 50, 1214), 4);
    }