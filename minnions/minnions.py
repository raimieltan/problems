#code template
def nb_year(population, percent, aug, target):
    #code here

#solution
def nb_year(population, percent, berks, target):
    year = 0
    while population < target:
        population += population * percent / 100. + berks
        year += 1
    return year

#test cases

def test___SE___case_1(self):
    self.assertEqual(nb_year(1500, 5, 100, 5000), 15)

def test___SE___case_2(self):
    self.assertEqual(nb_year(1500000, 2.5, 10000, 2000000), 10)

def test___SE___case_3(self):
    self.assertEqual(nb_year(1500000, 0.25, 1000, 2000000), 94)

def test___SE___case_4(self):
    self.assertEqual(nb_year(1500000, 0.25, -1000, 2000000), 151)

def test___SE___case_5(self):
    self.assertEqual(nb_year(1500000, 0.25, 1, 2000000), 116)

def test___SE___case_6(self):
    self.assertEqual(nb_year(1500000, 0.0, 10000, 2000000), 50)