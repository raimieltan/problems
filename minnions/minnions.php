//solution
function nbYear($population, $percent, $berks, $target) {
  $count = 0;
  while ($population < $target) {
    $population *= 1 + $percent / 100;
    $population += $berks;
    $count++;
  }
  return $count;
}

//template
function nbYear($population, $percent, $berks, $target) {

}

//test cases
public function test___SE___case_1() {
        $this->assertSame(nbYear(1500, 5, 100, 5000), 15);
}
public function test___SE___case_2() {
        $this->assertSame(nbYear(1500000, 2.5, 10000, 2000000), 10);
    }
public function test___SE___case_3() {
        $this->assertSame(nbYear(1500000, 0.25, 1000, 2000000), 94);
    }
public function test___SE___case_4() {
        $this->assertSame(nbYear(1500000, 0.25, -1000, 2000000), 151);
    }
public function test___SE___case_5() {
        $this->assertSame(nbYear(1500000, 0.25, 1, 2000000), 116);
    }
public function test___SE___case_6() {
        $this->assertSame(nbYear(1500000, 0.0, 10000, 2000000), 50);
    }
    