//solution
function nbYear(populationn, percent, berks, target) {
    for(let year = 0; populationn < target; year++) populationn = populationn * (1 + percent / 100) + berks;
    return year;
  }

//template
function nbYear(populationn, percent, berks, target) {
    //code here
  }

//test cases
  describe("nbYear",function() {
      
    it("test___SE___case_1",function() {    
        assert.strictEqual(nbYear(1500, 5, 100, 5000), 15);
    })
    it("test___SE___case_2",function() {   
        assert.strictEqual(nbYear(1500000, 2.5, 10000, 2000000), 10);
    })
    it("test___SE___case_3",function() {   
        assert.strictEqual(nbYear(1500000, 0.25, 1000, 2000000), 94);
    })
    it("test___SE___case_4",function() {   
        assert.strictEqual(nbYear(1500000, 0.25, -1000, 2000000), 151);
    })
    it("test___SE___case_5",function() {   
        assert.strictEqual(nbYear(1500000, 0.25, 1, 2000000), 116);
    })
    it("test___SE___case_6",function() {   

        assert.strictEqual(nbYear(1500000, 0.0, 10000, 2000000), 50);
    })})