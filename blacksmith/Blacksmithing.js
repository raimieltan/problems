class Blacksmithing {
    constructor(name){
        this.name = name
        this.items = []
        this.requirements = []
    }

    addItems(materials){
        this.items.push(materials)
    }

    addRequirements(materials){
        this.requirements.push(materials)
    }

    itemSort(items){
    
        function comparator (a, b) {
            if (a.name > b.name) {
                return -1;
            }
            if (b.name > a.name) {
                return 1;
            }
            return 0;
        }

        return items.sort(comparator)

    }

    craft(required){
     
        if(this.checkItems(required)){
            for(let i = 0; i < required.length; i++){
                
                if(required[i].amount !== this.items[i].amount){
                    return `${this.items[i].name} does not match the exact amount`
                }
            }
            return `You crafted a ${this.name}`

        }
        else{
            return `You failed crafting a ${this.name}`
        }
       
    }

    checkItems(required){
       
        this.itemSort(this.requirements)
        this.itemSort(this.items)

        if(this.requirements.length === this.items.length){
            let index = 0
            for(let items of this.items){
                if(items.name !== required[index].name){
                    return false
                }
    
                index++
            }
            return true
        }

        else{
            return 'You are missing some materials'
        }
       
    }

}

class Materials {

    constructor(name, amount){
        this.name = name;
        this.amount = amount
    }

}

const weapon = new Blacksmithing('Sword')
weapon.addRequirements(new Materials('Iron', 5))
weapon.addRequirements(new Materials('Wood', 2) )
weapon.addItems(new Materials('Wood', 2))
weapon.addItems(new Materials('Iron', 2))

console.log(weapon.craft(weapon.requirements))
// console.log(weapon.requirements)