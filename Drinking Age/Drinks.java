public class Drinks {

    public static String drinkers(int year, int month) {

        if (year >= 6) {
            year += 1;
        }

        String kids = "drink toddy";
        String teens = "drink coke";
        String young = "drink beer";
        String adults = "drink whisky";

        if (year < 14) {
            return kids;
        }
        if (year < 18) {
            return teens;
        }
        if (year < 21) {
            return young;
        }
        return adults;

    }

}