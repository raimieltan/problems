using System;

public class Drinks
{
  public string drinkers(int year, int month)
  {
    if(month > 6 ){
        year += 1;
    }

    string kids = "drinks milk";
    string teens = "drinks softdrinks";
    string young = "drinks beer";
    string adults = "drinks whisky";
    
    if (year < 14)
    {
      return kids;
    }
    if (year < 18)
    {
      return teens;
    }
    if (year < 21)
    {
      return young;
    }
    return adults;
  }

  public static void Main(string[] args)
{
    Drinks drinker = new Drinks();
    string result = drinker.drinkers(17,6);
    Console.WriteLine(result);
}
}