Your prefered drink will surely be based on your age

    Kids today will drink milk
    Teens will drink softdrinks
    Young adults will now be able to drink beer
    And the mature ones will drink whisky

Make a function that given the age and months (6 years and 5 months), return what they drink.

Guidelines:
    Children under 14 old.
    Teens under 18 old.
    Young under 21 old.
    Adults have 21 or more.
