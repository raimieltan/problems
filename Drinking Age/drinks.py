def drinkers(year,month):
    if month >= 6:
        year += 1
    
    if year > 20: return 'drinks whisky'
    if year > 17: return 'drinks beer'
    if year > 13: return 'drinks softdrinks'
    return 'drinks milk'

print(drinkers(17,6))