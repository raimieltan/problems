<?php
function drinkers(int $year, int $month): string {

  if($month >= 6){
      $year += 1;
  }

  if ($year < 14) {
    return "drinks milk";
  } else if ($year < 18) {
    return "drinks softdrinks";
  } else if ($year < 21) {
    return "drinks beer";
  }
  return "drinks whisky";
}

echo (drinkers(17,6));
?>